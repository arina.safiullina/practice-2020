# Производственная практика 2020
## Инвариантные задания

1. [Задание 1.1](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/1_1.pdf)

2. [Задание 1.2](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/1_2.pdf)

3. [Задание 1.3](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/1_3.pdf)

4. [Задание 1.4](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/1_4.pdf)

## Вариативные задания

1. [Задание 2.1](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/2_1.pdf)

2. [Задание 2.2](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/2_2.pdf)

## Документы по практике

[Отчет](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/%D0%BE%D1%82%D1%87%D0%B5%D1%82_%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D0%BA%D0%B0.docx)

[Задание](https://gitlab.com/arina.safiullina/practice-2020/-/blob/master/%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%BA%D1%82.docx)